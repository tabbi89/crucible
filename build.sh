#!/bin/bash

echo "Building atlassian-base..." && docker build -t "mswinarski/atlassian-base:1.8" ./base/1.8/

echo "Building atlassian-fisheye latest..." && docker build -t "mswinarski/atlassian-fisheye:latest" ./fisheye/4.5/
echo "Building atlassian-crucible latest..." && docker build -t "mswinarski/atlassian-crucible:latest" ./crucible/4.5/
