# Installing FishEye and Crucible on AWS

1. Launch an EC2 instance from Amazon Linux AMI, SSD Volume Type
2. Select appropriate type (like: m3.xlarge)
3. Configure instance: select or create VPC and subnet and in "Advanced Details" select the "install_fecru.sh"
4. Add storage: advising to have a separated EBS (Provisioned IOPS) storage for FISHEYE_INST
5. Tag instance:
6. Select or create security group with: inbound ssh and http:80 open

Launch the instance!
